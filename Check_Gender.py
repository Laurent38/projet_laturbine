import database
import gender_guesser.detector as gender
import scrap_google as sc
import json
from urllib.request import urlopen


def select_en_base():
    debug = 1
    conn = database.create_connection()

    cur = database.query_create_select(conn, "Select * From nom_des_voies;")

    stopwords = ['la', 'le', 'des', 'de', 'Père', 'point', 'Saint', 'Place',
                 'Rue', 'Avenue', 'Allée', 'Quai', 'Rond', 'Chemin', 'Passage', 'Cours',
                 'Boulevard', 'Impasse', 'Général', 'Lieutenant', 'Route', 'Cour', 'Galerie',
                 'Président', 'Prosper', 'ème', 'Régiment', 'Jardin', 'Champ', 'La', 'Le',
                 'et', 'Lys', 'Docteur', 'ter', 'Capitaine', 'Parc', 'Square', 'Stade', 'bis',
                 'Voie', 'Pont', 'Commandant', 'Sainte', 'Colonel', 'Espace']

    for ligne in cur:
        try:
            if debug == 1:
                print(ligne[0], ligne[1])
            prenom = ligne[1].split(" ")[1]
            if prenom in stopwords:
                prenom = ligne[1].split(" ")[2]
                if prenom in stopwords:
                    prenom = ligne[1].split(" ")[3]

            nom = ligne[1].split(" ")[2]
            if debug == 1:
                print(prenom, nom)
            d = sc.check_genre(prenom)
#            data = d.get_gender(prenom)
            if debug == 1:
                print(f'prenom :{prenom} genre:{d}')

            # Mise à jour du genre en base données
            requete = "update nom_des_voies set genre = '" + d + "' Where voie_id = " + str(ligne[0]) + ";"
       #     try:
      #          database.query_create_select(conn, requete)
       #     except:
       #         print("Erreur")

            if debug == 1:
                print(requete)

        except IndexError:
            print("Juste un nom dans la rue")
            print(ligne[1])
            d = gender.Detector()
            data = d.get_gender(prenom)
            if debug == 2:
                print(f'prenom :{prenom} genre:{data}')

            # Mise à jour du genre en base données
            requete = "update nom_des_voies set genre = '" + data + "' Where voie_id = " + str(ligne[0]) + ";"



def update_genre():
    conn = database.create_connection()

    cur = database.query_create_select(conn, "Select voie_id, lower(voie_complet) From nom_des_voies order by voie_id;")

    for ligne in cur:
        ligne_str = str(ligne[1])
        print(ligne_str,  check_genre(ligne_str, False))

        trouve = False
        if trouve:
            requete = "update nom_des_voies set genre = '{}' where voie_id = {};".format(genre, voie_id)
            print(requete)
            cur3 = database.query_create_select(conn, requete)


def check_genre(rue_a_checker, debug):

    conn = database.create_connection()

    mot_type_rue = ['rue', 'impasse', 'allée', 'avenue', 'boulevard', 'chemin', 'passage', 'parc', 'parvis', 'place',
                    'espace', 'montée', 'square', 'esplanade', 'traverse', 'voie', 'quai', 'du', 'de', 'la', 'des',
                    'grand']

    mot_type_masculin = ['général', 'maréchal', 'commandant', 'président', 'docteur', 'colonel', 'abbé', 'abbe']
    mot_type_feminin = ['générale', 'maréchale', 'commandante', 'présidente', 'docteuresse']

    trouve = False
    genre = ''

    tab_mot = rue_a_checker.split(" ")
    if debug==1: print(rue_a_checker)

    # On commence par regarder s'il y a un mot clé annonçant le genre (général, commandant...)
    for mot in tab_mot:
        if mot.lower() in mot_type_masculin:
            genre = 'masculin'
            trouve = True
        elif mot.lower() in mot_type_feminin:
            genre = 'feminin'
            trouve = True

    if not trouve:
        for mot in tab_mot:
            if mot.lower() not in mot_type_rue:
                mot = mot.replace("'", "''")
                if debug==1: print(mot)
                requete = "Select genre From prenoms where prenom = '" + mot.lower() + "' and (langage like '%french%' or langage like '%english%');"
                if debug==1: print(requete)
                cur2 = database.query_create_select(conn, requete)

                if cur2 is not None:
                    for ligne in cur2:
                        genre = ''.join(ligne)
                        trouve = True

    return trouve, genre


def check_gender_api(nom_a_checker):
    myKey = "UHpbZEfqcJwXBQzXwV"
    url = "https://gender-api.com/get?key=" + myKey + "&name=" + nom_a_checker.replace(" ", "%20")
    response = urlopen(url)
    decoded = response.read().decode('utf-8')
    data = json.loads(decoded)
    return data["gender"]




#print(check_gender_api('Jacques Chirac'))
update_genre()
#print(check_genre('rue George Sand', False))
#print(check_genre('rue Alphonse Daudet', False))

# select_en_base()