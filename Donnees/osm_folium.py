# -*- coding: utf-8 -*-
import requests
from lxml import etree
import folium

"""
Retourne les coordonnées GPS d'un noeud OSM à partir de son id
"""


def get_node_coords(id):
    response = requests.get(
        "https://api.openstreetmap.org/api/0.6/node/" + str(id), verify=True)

    if response.status_code == 200:
        root = etree.fromstring(response.content)
        node_list = root.xpath("/osm/node")
        for node in node_list:
            return (float(node.get("lat")), float(node.get("lon")))


"""
Saisie des informations
"""
street = input("Nom de la rue ? ")
city = input("Ville ? ")
country = input("Pays ? ")

"""
Géocodage avec l'API nominatim d'OSM
https://nominatim.org/release-docs/latest/api/Overview/
"""
parameters = {
    "street": street,
    "city": city,
    "country": country,
    "format": "json",
    "polygon": 1
}
# verify=False => avoid SSL certificate verification
response = requests.get(
    "https://nominatim.openstreetmap.org/search", parameters, verify=True)

if response.status_code == 200:
    info = response.json()
    print(response.request)

    # Centre de la carte Folium
    center = [float(info[0]['lat']), float(info[0]['lon'])]

    """
    Recherche des détails d'une portion de rue à partir de son identifiant OSM
    """
    response = requests.get(
        "https://api.openstreetmap.org/api/0.6/way/" + str(info[0]['osm_id']), verify=True)

    if response.status_code == 200:
        points = []

        """
        Recherche des noeuds associés à cette portion de rue
        """
        root = etree.fromstring(response.content)
        node_list = root.xpath("/osm/way/nd")
        for node in node_list:
            """
            Recherche des autres portions de rue connectées à ce noeuds
            """
            response = requests.get(
                "https://api.openstreetmap.org/api/0.6/node/" + node.get("ref") + "/ways", verify=True)

            if response.status_code == 200:
                root2 = etree.fromstring(response.content)
                """
                Ajout de tous les noeuds trouvés dans une liste
                """
                node_list2 = root2.xpath("/osm/way/nd")
                for node2 in node_list2:
                    points.append(get_node_coords(node2.get("ref")))

        """
        Création de la carte et d'une ligne polygonale à partir de
        la liste de points
        """
        fmap = folium.Map(
            location=center, tiles="OpenStreetMap", zoom_start=16)
        folium.PolyLine(points, color="blue", weight=2.5,
                        opacity=0.8).add_to(fmap)

        """
        Sauvegarde de la carte dans un fichier HTML
        """
        fmap.save("street4.html")