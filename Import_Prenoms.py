import database


def quote(texte):
    return "'{}'".format(texte)


def import_prenom_féminin():

    mon_fichier = open("./Donnees/Prenoms_feminins.txt", "r")

    lignes = mon_fichier.readlines()
    mon_fichier.close()

    for ligne in lignes:
        conn = database.create_connection()

        prenom_en_cours = ligne.strip().lower()
        print("prenom_en_cours: ", prenom_en_cours)

        if " : " in prenom_en_cours:
            print('On split')
            tab_split1 = prenom_en_cours.split(" : ")
            prenom_a_inserer = tab_split1[0]
            print('prenom_a_inserer: ', prenom_a_inserer)

            if " ou " in prenom_a_inserer:
                print("prenom_a_inserer1: ", prenom_a_inserer)
                tab_split2 = prenom_a_inserer.split(" ou ")
                for prenom in tab_split2:
                    print(prenom)
                    requete = "insert into prenoms(prenom, genre, langage, fréquence) values(" + quote(prenom) + ", 'feminin', 'french', 1.0);"
                    print(requete)
                    database.query_create_select(conn, requete)
            else:
                print("prenom_a_inserer2: ", prenom_a_inserer)
                requete = "insert into prenoms(prenom, genre, langage, fréquence) values(" + quote(prenom_a_inserer) + ", 'feminin', 'french', 1.0);"
                print(requete)
                database.query_create_select(conn, requete)

        else:
            if " ou " in prenom_en_cours:
                print("prenom_en_cours3: ", prenom_en_cours)
                tab_split2 = prenom_en_cours.split(" ou ")
                for prenom in tab_split2:
                    print(prenom)
                    requete = "insert into prenoms(prenom, genre, langage, fréquence) values(" + quote(prenom) + ", 'feminin', 'french', 1.0);"
                    print(requete)
                    database.query_create_select(conn, requete)
            else:
                print("prenom_en_cours4: ", prenom_en_cours)
                requete = "insert into prenoms(prenom, genre, langage, fréquence) values(" + quote(prenom_en_cours) + ", 'feminin', 'french', 1.0);"
                print(requete)
                database.query_create_select(conn, requete)

def import_prenom_masculin():

    mon_fichier = open("./Donnees/Prenoms_masculin.txt", "r")

    lignes = mon_fichier.readlines()
    mon_fichier.close()

    for ligne in lignes:
        conn = database.create_connection()

        prenom_en_cours = ligne.strip().lower()
        print("prenom_en_cours: ", prenom_en_cours)

        if " : " in prenom_en_cours:
            print('On split')
            tab_split1 = prenom_en_cours.split(" : ")
            prenom_a_inserer = tab_split1[0]
            print('prenom_a_inserer: ', prenom_a_inserer)

            if " ou " in prenom_a_inserer:
                print("prenom_a_inserer1: ", prenom_a_inserer)
                tab_split2 = prenom_a_inserer.split(" ou ")
                for prenom in tab_split2:
                    print(prenom)
                    requete = "insert into prenoms(prenom, genre, langage, fréquence) values(" + quote(prenom) + ", 'masculin', 'french', 1.0);"
                    print(requete)
                    database.query_create_select(conn, requete)
            else:
                print("prenom_a_inserer2: ", prenom_a_inserer)
                requete = "insert into prenoms(prenom, genre, langage, fréquence) values(" + quote(prenom_a_inserer) + ", 'masculin', 'french', 1.0);"
                print(requete)
                database.query_create_select(conn, requete)

        else:
            if " ou " in prenom_en_cours:
                print("prenom_en_cours3: ", prenom_en_cours)
                tab_split2 = prenom_en_cours.split(" ou ")
                for prenom in tab_split2:
                    print(prenom)
                    requete = "insert into prenoms(prenom, genre, langage, fréquence) values(" + quote(prenom) + ", 'masculin', 'french', 1.0);"
                    print(requete)
                    database.query_create_select(conn, requete)
            else:
                print("prenom_en_cours4: ", prenom_en_cours)
                requete = "insert into prenoms(prenom, genre, langage, fréquence) values(" + quote(prenom_en_cours) + ", 'masculin', 'french', 1.0);"
                print(requete)
                database.query_create_select(conn, requete)

import_prenom_masculin()
import_prenom_féminin()