import database as db
import numpy as np
import pandas as pd

def echange_en_base():
    requete = "select distinct voie_id, voie_complet, genre, geojson from nom_des_voies Where ville in ('Grenoble', 'Meylan') And geojson <> '{\"type\":\"LineString\",\"coordinates\":[]}';"
    print(requete)
    conn = db.create_connection()

    cur = db.query_create_select(conn, requete)

    if cur is not None:
        for ligne in cur:
            print("geojson: ", ligne[3])

            # on localise le mot clé coordinates
            tab_coord_in = ligne[3].split("],[")
            index = 1
            taille_coord_in = len(tab_coord_in)
            ligne_coord = ""

            for coord in tab_coord_in:
                print("coord: {}, index: {}".format(coord, index))
                if index == 1:
                    tab_coord2 = coord.split(":[[")
                    tab_coord3 = tab_coord2[1].split("], [")
                    tab_coord_out = np.zeros(len(tab_coord3))
                    ligne_str = ""

                    for i in range(0, len(tab_coord3)):
                        geojson = tab_coord2[1].split("], [")[i].split(", ")[0], tab_coord2[1].split("], [")[i].split(", ")[1]

                        if i == len(tab_coord3)-1:
                            tab_coord3[i] = tab_coord3[i][:len(tab_coord3[i])-3]

                      #  print("Dans For", str(tab_coord2[1].split("], [")[i].split(", ")[1]), str(tab_coord2[1].split("], [")[i].split(", ")[0]))

                        ligne_str = ligne_str + '[' + str(tab_coord2[1].split("], [")[i].split(", ")[1]) + ', ' + str(tab_coord2[1].split("], [")[i].split(", ")[0]) + '], '

                    ligne_coord = ligne_coord + '[' + ligne_str[:-2] + ']'

                elif index < taille_coord_in:
                #    tab_coord_out[index] = ("(" + coord + ")")
                    tab_coord_out[index] = coord
        #           print("coord{}: ".format(str(index)))
                index += 1

            print("n°: {}, voie: {}, ligne_coord: {}, genre {}".format(ligne[0], ligne[1], ligne_coord, ligne[2]))


def echange_dans_fichier():
    df = pd.read_csv('/home/laurent/git/turbine_project/Donnees/Doc_Villes/PLANDEVILLE_VOIES_VDG.csv', sep=';')

    conn = db.create_connection()

    # Dessin de la rue N°67 : Avenue Jean Perrot
    for i in range(1, 928):
        ligne_csv_finale = ''
        print("Id: {}, Rue: {} GeoJSON: {} ligne: {}".format(df['VOIE_ID'][i], df['VOIE_COMPLET'][i], df['GeoJSON'][i], i))
        try:
            tab_coord_in = df['GeoJSON'][i].split(":[[[")
        except Exception as inst:
            print("Erreur", inst)

        print("tab_coord_in: ", tab_coord_in, len(tab_coord_in))

        # Les coordonnées geojson commence par [[[

        if len(tab_coord_in) > 1:
            debut = ':[[['
            fin = ']]]}'
            tab_coord_in_droite = tab_coord_in[1].split("],[")

            for coord in tab_coord_in_droite:
                tab_coord_in_point_virgule = coord.split(";")
                print("tab_coord_in_point_virgule: {}".format(tab_coord_in_point_virgule))
                compteur = 1
                for cpv1 in reversed(tab_coord_in_point_virgule):
                    resultat = cpv1
                    if resultat[-4:] == ']]]}':
                        resultat = resultat[0:len(resultat)-4]

                    if compteur == 2:
                        texte_final = texte_final + resultat + '], '
                        compteur = 1
                    else:
                        texte_final = texte_final + '[' + resultat + ";"
                        compteur += 1

                    print("coord[[[: {}".format(resultat))

            texte_final = texte_final[:len(texte_final) - 2]

        # Sinon elles commencent par [[
        else:
            tab_coord_in = df['GeoJSON'][i].split(":[[")
            debut = ':[['
            fin = ']]}'
            if len(tab_coord_in) > 1:
                tab_coord_in_droite = tab_coord_in[1].split("],[")
                texte_final = ''

                for coord in tab_coord_in_droite:
                    tab_coord_in_point_virgule = coord.split(";")
                    compteur = 1
                    for cpv in reversed(tab_coord_in_point_virgule):
                        resultat = cpv
                        if resultat[-3:] == ']]}':
                            resultat = resultat[0:len(resultat) - 3]

                        if compteur == 2:
                            texte_final = texte_final + resultat + '], '
                            compteur = 1
                        else:
                            texte_final = texte_final + '[' + resultat + ";"
                            compteur += 1


                        print("coord[[: {}".format(resultat))
                texte_final = texte_final[:len(texte_final)-2]

        texte_final = tab_coord_in[0] + debut + texte_final + fin
        print("Id: {}, Rue: {} GeoJSON: {}".format(df['VOIE_ID'][i], df['VOIE_COMPLET'][i], texte_final))
        requete = "update nom_des_voies set geojson = " + db.quote(texte_final) + " Where voie_id = " + str(df['VOIE_ID'][i]) + ";"
        print('requete: ' + requete)
#        try:
#            cur = db.query_create_select(conn, requete)
#        except Exception as e:
#            print(e)

def echange_en_base_depuis_save():
    requete = "select distinct voie_id, geojson from nom_des_voies_save Where geojson <> '{\"type\":\"LineString\",\"coordinates\":[]}' order by voie_id;"
    print(requete)
    conn = db.create_connection()

    cur = db.query_create_select(conn, requete)

    if cur is not None:
        for ligne in cur:
            print("Id: {} geojson: {}".format(ligne[0], ligne[1]))
            print(ligne[1])

            try:
                tab_coord_in = ligne[1].split(":[[")

                tab_split1 = tab_coord_in[1].split("], [")
                chaine_insersee = ""
                for chaine in tab_split1:
                    tab_split2 = chaine.split(", ")
                    coordonnees_inversees = ""
                    for chaine2 in reversed(tab_split2):
                        if chaine2.find(']]}') >= 0:
                            chaine2 = chaine2[:-3]

                        coordonnees_inversees += chaine2 + ', '
                    chaine_insersee = chaine_insersee + '[' + coordonnees_inversees[:-2] + '], '
        #        print(chaine_insersee[:-2])
                chaine_finale = tab_coord_in[0] + '[' + chaine_insersee[:-2] + ']}'
                print(chaine_finale)

                requete_update = 'Update nom_des_voies set geojson = ' + db.quote(chaine_finale) + ' where voie_id = ' + str(ligne[0]) + ';'
                print(requete_update)


            except Exception as inst:
                print("Erreur", inst)

def echange_en_base_ville(ville):
    requete = "select distinct voie_id, geojson from nom_des_voies_save Where ville = '" + ville + "' and geojson <> '{\"type\":\"LineString\",\"coordinates\":[]}' order by voie_id;"
    print(requete)
    conn = db.create_connection()

    cur = db.query_create_select(conn, requete)

    if cur is not None:
        for ligne in cur:
            print("Id: {} geojson: {}".format(ligne[0], ligne[1]))
            print(ligne[1])

            try:
                tab_coord_in = ligne[1].split(":[[")

                tab_split1 = tab_coord_in[1].split("], [")
                chaine_insersee = ""
                for chaine in tab_split1:
                    tab_split2 = chaine.split(", ")
                    coordonnees_inversees = ""
                    for chaine2 in reversed(tab_split2):
                        if chaine2.find(']]}') >= 0:
                            chaine2 = chaine2[:-3]

                        coordonnees_inversees += chaine2 + ', '
                    chaine_insersee = chaine_insersee + '[' + coordonnees_inversees[:-2] + '], '
        #        print(chaine_insersee[:-2])
                chaine_finale = tab_coord_in[0] + '[' + chaine_insersee[:-2] + ']}'
                print(chaine_finale)

                requete_update = 'Update nom_des_voies set geojson = ' + db.quote(chaine_finale) + ' where voie_id = ' + str(ligne[0]) + ';'
                print(requete_update)

                db.query_create_select(conn, requete_update)


            except Exception as inst:
                print("Erreur", inst)


#echange_en_base()
#echange_dans_fichier()
#echange_en_base_depuis_save()
#echange_en_base_ville('La Tronche')
#echange_en_base_ville('Meylan')
#echange_en_base_ville('Eybens')
#echange_en_base_ville('Echirolles')
#echange_en_base_ville('Saint_Martin_dHeres')
echange_en_base_ville('Seyssinet-Pariset')
