import pandas as pd
import json
import folium
import database
from flask import Flask

# Dessine une line polygonale à partir des coordonnées GPS contenues
# dans l'objet GeoJSON
def draw_polyline(geo_json, map, color="blue", weight=5, opacity=0.6):
    data = json.loads(geo_json)

    if data['type'] == 'LineString':
        points = []
        for coord in data['coordinates']:
            points.append((coord[1], coord[0]))
        folium.PolyLine(points, color=color, weight=weight, opacity=opacity).add_to(map)

    if data['type'] == 'MultiLineString':
        for line in data['coordinates']:
            points = []
            for coord in line:
                points.append((coord[1], coord[0]))
            folium.PolyLine(points, color=color, weight=weight, opacity=opacity).add_to(map)


app = Flask(__name__)

@app.route('/')
def index():

    # Création de la connexion à la base de données
    conn = database.create_connection()

    # Création d'une carte centrée sur Grenoble
    fmap = folium.Map(location=[45.1875602, 5.7357819], tiles="OpenStreetMap", zoom_start=13)

    lgd_txt = '<span style="color: {col};">{txt}</span>'

    group_routes_masculines = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Masculines", col="blue"))
    group_routes_feminines = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Féminines", col="red"))
    group_routes_neutres = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Neutres", col="green"))
    group_ecoles_feminines = folium.FeatureGroup(name=lgd_txt.format(txt="Ecoles (Fem.)", col="purple"))
    group_ecoles_masculines = folium.FeatureGroup(name=lgd_txt.format(txt="Ecoles (Masc.)", col="blue"))
    group_ecoles_neutres = folium.FeatureGroup(name=lgd_txt.format(txt="Ecoles Neutres", col="gray"))
    group_routes_masculines.add_to(fmap)
    group_routes_feminines.add_to(fmap)
    group_routes_neutres.add_to(fmap)
    group_ecoles_feminines.add_to(fmap)
    group_ecoles_masculines.add_to(fmap)
    group_ecoles_neutres.add_to(fmap)

    requete  = "select distinct voie_id, voie_complet, ville, genre,  geojson from nom_des_voies Where geojson <> " \
               "'{\"type\":\"LineString\",\"coordinates\":[]}';"

    cur = database.query_create_select(conn, requete)

    for ligne in cur:
        genre = ligne[3]
        #print(ligne[2])

        print(ligne)

        try:

            if ligne[4] is not None:
                #print('voie: {} Geojson: {}'.format(ligne[0], ligne[4]))
                if genre == "masculin":
                    draw_polyline(ligne[4], group_routes_masculines)
                elif genre == "feminin":
                    draw_polyline(ligne[4], group_routes_feminines, color="red")
                else:
                    draw_polyline(ligne[4], group_routes_neutres, color="green")

        except Exception as inst:
                print("Erreur", inst)


    # Initialisation du DataFrame df
    # à partir des données du fichier CSV
    df = pd.read_csv('/home/laurent/git/turbine_project/Donnees/Doc_Villes/PLANDEVILLE_VOIES_VDG.csv', sep=';')

    # Dessin de la rue N°67 : Avenue Jean Perrot
    print(df['GeoJSON'][67])
    draw_polyline(df['GeoJSON'][67], group_routes_masculines)

    # Sauvegarde de la carte dans un fichier HTML
    fmap.save("street.html")

    return fmap._repr_html_()


if __name__ == '__main__':
    app.run(port=5003)
