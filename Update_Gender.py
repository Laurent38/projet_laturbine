import database

# On crée la connexion à  la base de données
conn = database.create_connection()

# On charge les points d'intérêts depuis la base de données
cur = database.query_create_select(conn, "select voie_id, voie_complet, genre from nom_des_voies where genre = '';") # where geojson not like '%[[[%';")

liste_masculin = []
liste_feminin = []
liste_neutre = []

# Pour toutes les lignes de la table
compteur = 0
for ligne in cur:
    genre = input("Genre de " + ligne[1] + " ? (m = masculin, f = féminin, n = neutre)")
    if genre == "m":
        liste_masculin.append(ligne[0])
    elif genre == "f":
        liste_feminin.append(ligne[0])
    elif genre == "n":
        liste_neutre.append(ligne[0])

    print("liste_masculin: ", liste_masculin)
    print("liste_feminin: ", liste_feminin)
    print("liste_neutre: ", liste_neutre)

    requete_masculin = "UPDATE nom_des_voies set genre = 'masculin' where voie_id in ("

    if liste_masculin:
        for elt in liste_masculin:
            requete_masculin = requete_masculin + str(elt) + ", "

        requete_masculin = requete_masculin[0:len(requete_masculin) - 2] + ");"

        print("requete: ", requete_masculin)

        cur2 = database.query_create_select(conn, requete_masculin)

    requete_feminin = "UPDATE nom_des_voies set genre = 'feminin' where voie_id in ("

    if liste_feminin:
        for elt in liste_feminin:
            requete_feminin = requete_feminin + str(elt) + ", "

        requete_feminin = requete_feminin[0:len(requete_feminin) - 2] + ");"

        print("requete: ", requete_feminin)



