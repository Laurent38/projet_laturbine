from flask import Flask
import folium
import database
import pandas as pd
import json

app = Flask(__name__)

# on charge la base de données centrée sur Grenoble
m = folium.Map(location=[45.1875602, 5.7357819], tiles="OpenStreetMap", zoom_start=13.5)
folium.raster_layers.TileLayer('Open Street Map').add_to(m)
folium.raster_layers.TileLayer('Stamen Terrain').add_to(m)
folium.raster_layers.TileLayer('Stamen Toner').add_to(m)
folium.raster_layers.TileLayer('Stamen Watercolor').add_to(m)
folium.raster_layers.TileLayer('CartoDB Positron').add_to(m)
folium.raster_layers.TileLayer('CartoDB Dark_Matter').add_to(m)

# On crée la connexion à  la base de données
conn = database.create_connection()

# On charge les points d'intérêts depuis la base de données
cur = database.query_create_select(conn, "select * from coord_points_interets;")

# On crée la dataframe contenant les données
# Element 6 et 7 du curseur: coordonnées du point

latitude = []
longitude = []
texte_point_interet = []
titre = []
genre = []

for ligne in cur:
    latitude.append(ligne[7])
    longitude.append(ligne[6])
    texte_point_interet.append("<a href=" + ligne[11] + "" " target=""_blank>" + ligne[11] + "</a>" + '\n' + ligne[8])
    titre.append(ligne[2])
    genre.append(ligne[14])


# Maintenant on construit le dataframe pour la fonction folium.Marker

data = pd.DataFrame({
    'lat': latitude,
    'lon': longitude,
    'titre': titre,
    'name': texte_point_interet,
    'genre': genre
})

# On crée le groupe "Points d'Intérêt"
lgd_txt = '<span style="color: {col};">{txt}</span>'

group01_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Art et Culture (fem.)", col="darkgreen"))
group01_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Art et Culture (masc.)", col="green"))
group02_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Sciences et Techniques (fem.)", col="darkpurle"))
group02_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Sciences et Techniques (masc.)", col="purle"))
group03_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Patrimoine Naturel(fem.)", col="red"))
group03_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Patrimoine Naturel (masc.)", col="darkred"))
group04_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Histoire et Evolution (fem.)", col="darkblue"))
group04_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Histoire et Evolution (masc.)", col="blue"))

# Maintenant on associe les points d'intérêt à la carte
for i in range(0, len(data)):
    # print(data)
    # On colore differemment les points d'intérets s'ils sont à consonnance masculine ou  féminine
    if (data.iloc[i]['titre'].find("Sciences/techn") > 0):
        if (data.iloc[i]['genre'] == "masculin"):  # COULEUR A CONSONNANCE NOM MASCULIN
            folium.Marker([data.iloc[i]['lon'], data.iloc[i]['lat']], popup=data.iloc[i]['name'][0:200],
                          icon=folium.Icon(color='darkgreen', icon='info-sign')).add_to(group01_masc)
        elif (data.iloc[i]['genre'] == "feminin"):  # COULEUR A CONSONNANCE NOM FEMININ
            folium.Marker([data.iloc[i]['lon'], data.iloc[i]['lat']], popup=data.iloc[i]['name'][0:200],
                          icon=folium.Icon(color='green', icon='info-sign')).add_to(group01_fem)

    elif (data.iloc[i]['titre'].find("Patrimoine nat") > 0):
        if (data.iloc[i]['genre'] == "masculin"):  # COULEUR A CONSONNANCE NOM MASCULIN
            folium.Marker([data.iloc[i]['lon'], data.iloc[i]['lat']], popup=data.iloc[i]['name'][0:200],
                          icon=folium.Icon(color='darkred', icon='info-sign')).add_to(group03_masc)
        elif (data.iloc[i]['genre'] == "feminin"):  # COULEUR A CONSONNANCE NOM FEMININ
            folium.Marker([data.iloc[i]['lon'], data.iloc[i]['lat']], popup=data.iloc[i]['name'][0:200],
                          icon=folium.Icon(color='red', icon='info-sign')).add_to(group03_fem)

    elif (data.iloc[i]['titre'].find("Histoire & Evo") > 0):
        if (data.iloc[i]['genre'] == "masculin"):  # COULEUR A CONSONNANCE NOM MASCULIN
            folium.Marker([data.iloc[i]['lon'], data.iloc[i]['lat']], popup=data.iloc[i]['name'][0:200],
                          icon=folium.Icon(color='darkblue', icon='info-sign')).add_to(group04_fem)
        elif (data.iloc[i]['genre'] == "feminin"):  # COULEUR A CONSONNANCE NOM FEMININ
            folium.Marker([data.iloc[i]['lon'], data.iloc[i]['lat']], popup=data.iloc[i]['name'][0:200],
                          icon=folium.Icon(color='blue', icon='info-sign')).add_to(group04_masc)
    elif (data.iloc[i]['titre'].find("Evolution de la ville") > 0):
        if (data.iloc[i]['genre'] == "masculin"):  # COULEUR A CONSONNANCE NOM MASCULIN
            folium.Marker([data.iloc[i]['lon'], data.iloc[i]['lat']], popup=data.iloc[i]['name'][0:200],
                          icon=folium.Icon(color='darkpurple', icon='info-sign')).add_to(group02_fem)
        elif (data.iloc[i]['genre'] == "feminin"):  # COULEUR A CONSONNANCE NOM FEMININ
            folium.Marker([data.iloc[i]['lon'], data.iloc[i]['lat']], popup=data.iloc[i]['name'][0:200],
                          icon=folium.Icon(color='purple', icon='info-sign')).add_to(group02_masc)

# On ajoute les groupes à la carte
group01_fem.add_to(m)
group01_masc.add_to(m)
group02_fem.add_to(m)
group02_masc.add_to(m)
group03_fem.add_to(m)
group03_masc.add_to(m)
group04_fem.add_to(m)
group04_masc.add_to(m)

# group1 = folium.FeatureGroup(name='<span style=\\"color: red;\\">Routes Féminines</span>')              # (, show=False) a ajouter pour que la checkbox soit décochée au départ
group1 = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Féminines", col="red"))

# On va chercher les données ne base concernant les rues dont le nom est féminin
cur = database.query_create_select(conn, "select genre, geojson From nom_des_voies where geojson not like '%[[[%' and genre = 'feminin';")

for ligne in cur:
    geojson = ligne[1]

    couleur = "red" # On affiche les rues dont le nom est féminin
    data = json.loads(geojson)

    if data['type'] == 'LineString':
        points = []

        for coord in data['coordinates']:
            points.append((coord[1], coord[0]))

            folium.PolyLine(points, color='red', weight=5, opacity=0.6).add_to(group1)

    if data['type'] == 'MultiLineString':
        for line in data['coordinates']:
            points = []
            for coord in line:
                points.append((coord[1], coord[0]))

            folium.PolyLine(points, color='red', weight=5, opacity=0.6).add_to(group1)

group1.add_to(m)

# group2 = folium.FeatureGroup(name='<span style=\\"color: blue;\\">Routes Masculines</span>')
group2 = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Masculines", col="blue"))


# On va maintenant chercher les données en base concernant les rues dont le nom est masculin
cur = database.query_create_select(conn, "select genre, geojson, nom_des_voies From nom_des_voies where geojson not like '%[[[%' and genre = 'masculin';")

for ligne in cur:
#    print(ligne)
    geojson = ligne[1]

#    print("geojson: ", geojson)
#    geojson = "{\"type\":\"MultiLineString\",\"coordinates\":[[[5.73364716776952,45.1833490915976],[5.73365648420878,45.183104221506],[5.73380659712687,45.1829987912521],[5.73408063895113,45.1824687529321],[5.73431546049113,45.1820145641972],[5.73442103662995,45.1818002210245],[5.73460874096931,45.181419135052],[5.73464278272215,45.181350021144],[5.73469257004274,45.1812489391841],[5.73473335527602,45.1811661363776],[5.73484149572557,45.180946586752],[5.73500946430215,45.1806055689409],[5.73537359936673,45.1798662557243],[5.73551561641535,45.1795779033254],[5.73564441465872,45.1793164042969],[5.73573109461773,45.1791404098186],[5.73574055323101,45.1791212050366],[5.7360006506231,45.17859309759],[5.73656982236416,45.1774374029174],[5.73668803768429,45.1771973623058],[5.7370390102978,45.1764846927512],[5.73733088076805,45.1758920051698],[5.73761562399733,45.1753138047437],[5.73860450919768,45.1733056125307],[5.7386876015978,45.1731368666387],[5.73901076394597,45.1724805740298],[5.73904206181909,45.1724170120996],[5.73940227411225,45.1716854479694],[5.73954822951268,45.1713890188278],[5.73990506332672,45.1706642910497],[5.74008603004498,45.170296735017],[5.74067136048772,45.1691078675425],[5.74089712665689,45.1686492886598],[5.74146125270934,45.1675034331676]],[[5.74161987381538,45.1671812294014],[5.74188132238335,45.1666489304056]]]}"
#    geojson = "{\"type\":\"MultiLineString\",\"coordinates\":[[[5.72903804011517,45.1877083140852],[.73002109318556,45.1880655502734],[5.73125816953391,45.1885126965622]],[[5.7229452369528,45.1854939439964],[5.72402857532704,45.1858877081056],[5.72467716737003,45.1861234464337],[5.72750804934443,45.1871522984843],[5.72861213915014,45.1875535393851]]]}"
#    geojson = "{\"type\":\"MultiLineString\",\"coordinates\":[[5.72996019471978,45.1935892543962],[5.72992606191922,45.1934612897873],[5.72979651059553,45.1929755937918]]}"
#    print("geojson: ", geojson)


    couleur = "blue" # On affiche les rues dont le nom est masculin
    data = json.loads(geojson)

    if data['type'] == 'LineString':

        points = []

        for coord in data['coordinates']:
            points.append((coord[1], coord[0]))

            folium.PolyLine(points, color='blue', weight=5, opacity=0.6).add_to(group2)

    if data['type'] == 'MultiLineString':
        for line in data['coordinates']:
            points = []
            for coord in line:
                points.append((coord[1], coord[0]))

            folium.PolyLine(points, color='blue', weight=5, opacity=0.6).add_to(group2)

group2.add_to(m)

folium.LayerControl().add_to(m)
m.save("templates/street.html")