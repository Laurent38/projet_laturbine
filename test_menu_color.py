import folium

m = folium.Map((51, 6), tiles='stamentoner', zoom_start=7)

group0 = folium.FeatureGroup(name='<span style=\\"color: red;\\">red circles</span>')
for lat, lng in zip(range(500, 520), range(50, 70)):
    folium.CircleMarker((lat / 10, lng / 10), color='red', radius=2).add_to(group0)
group0.add_to(m)

group1 = folium.FeatureGroup(name='<span style=\\"color: blue;\\">blue circles</span>')
for lat, lng in zip(range(500, 520), range(70, 50, -1)):
    folium.CircleMarker((lat / 10, lng / 10), color='blue', radius=2).add_to(group1)
group1.add_to(m)

folium.map.LayerControl('topright', collapsed=False).add_to(m)

m.save("templates/street_menu_color.html")